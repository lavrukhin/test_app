
#ifndef LM75_H_
#define LM75_H_

#include <stdbool.h>
#include <stdint.h>


bool lm75_init();
int16_t lm75_get_temperature();


#endif /* LM75_H_ */
