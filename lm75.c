
#include "lm75.h"

#include "ti_drivers_config.h"

#include <ti/drivers/I2C.h>
#include <ti/display/Display.h>

#include <stdint.h>
#include <stddef.h>
#include <unistd.h>


#define DEVICE_ADDRESS  (0X48)
#define RESULT_REGISTER (0x0)


static I2C_Handle i2c = NULL;

bool lm75_init()
{
    uint8_t         txBuffer[1];
    uint8_t         rxBuffer[2];
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

    I2C_Params_init( &i2cParams );
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open( CONFIG_I2C_TMP, &i2cParams );
    if (i2c == NULL)
        return false;

    i2cTransaction.writeBuf   = txBuffer;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf    = rxBuffer;
    i2cTransaction.readCount  = 0;

    i2cTransaction.slaveAddress = DEVICE_ADDRESS;
    txBuffer[0] = RESULT_REGISTER;

    if (!I2C_transfer( i2c, &i2cTransaction ))
    {
        I2C_close( i2c );
        i2c = NULL;
    }
    return (i2c != NULL);
}

int16_t lm75_get_temperature()
{
    uint8_t         txBuffer[1];
    uint8_t         rxBuffer[2];
    I2C_Transaction i2cTransaction;
    int16_t         temperature = 0;

    if (i2c == NULL)
        return temperature;

    i2cTransaction.writeBuf   = txBuffer;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf    = rxBuffer;
    i2cTransaction.readCount  = 2;

    i2cTransaction.slaveAddress = DEVICE_ADDRESS;

    if (I2C_transfer( i2c, &i2cTransaction ))
    {
        temperature = (rxBuffer[0] << 3) + (rxBuffer[1] >> 5);
        temperature = temperature >> 3; // * 0.125
        if (rxBuffer[0] & 0x80)
            temperature |= 0xFF00;
    }

    return temperature;
}



