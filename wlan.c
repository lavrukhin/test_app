
#include "wlan.h"
#include "uart_term.h"
#include <ti/drivers/net/wifi/simplelink.h>


#define SL_STOP_TIMEOUT         (200)



int32_t wlan_start()
{
    int32_t ret = 0;

    const uint8_t ssid[] = "cc3220_ap";
    const uint8_t security_type = SL_WLAN_SEC_TYPE_WPA_WPA2;
    const uint8_t pass[] = "12345678";
    const uint8_t ssid_hidden = 0;
    const uint8_t channel = 6;
    const uint8_t tx_power = 0;

    // set SSID name
    ret = sl_WlanSet( SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_SSID, strlen( ssid ), ssid );
    if (ret < 0)
    {
        UART_PRINT( "Error: set ssid %d\n", ret );
        return ret;
    }

    // Set security type
    ret = sl_WlanSet( SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_SECURITY_TYPE, sizeof( security_type ), &security_type );
    if (ret < 0)
    {
        UART_PRINT( "Error: set security type %d\n", ret );
        return ret;
    }

    ret = sl_WlanSet( SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_PASSWORD, strlen( pass ), pass );
    if (ret < 0)
    {
        UART_PRINT( "Error: set password %d\n", ret );
        return ret;
    }

    ret = sl_WlanSet( SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_HIDDEN_SSID, sizeof( ssid_hidden ), &ssid_hidden );
    if (ret < 0)
    {
        UART_PRINT( "Error: set hidden ssid option %d\n", ret );
        return ret;
    }

    // Set channel number
    ret = sl_WlanSet( SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_CHANNEL, sizeof( channel ), &channel );
    if (ret < 0)
    {
        UART_PRINT( "Error: set channel %d\n", ret );
        return ret;
    }

    // Set TX power
    ret = sl_WlanSet( SL_WLAN_CFG_GENERAL_PARAM_ID, SL_WLAN_GENERAL_PARAM_OPT_AP_TX_POWER, sizeof( tx_power ), &tx_power );
    if (ret < 0)
    {
        UART_PRINT( "Error: set TX power %d\n", ret );
        return ret;
    }

    // Set device role as AP
    ret = sl_WlanSetMode( ROLE_AP );
    if (ret < 0)
    {
        UART_PRINT( "Error: start as AP %d\n", ret );
        return ret;
    }

    // Restart the NWP so the new configuration will take affect
    sl_Stop( SL_STOP_TIMEOUT );
    sl_Start( 0, 0, 0 );

    return ret;
}
