
#include "uart_term.h"
#include "lm75.h"

//#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <sys/socket.h>
#include <errno.h>

#include <ti/net/slnetutils.h>
#include <ti/drivers/net/wifi/sl_socket.h>

#include <string.h>
#include <stdint.h>



#define TELNET_PORT     (23)
#define NUMTCPWORKERS   (3)
#define TCPPACKETSIZE   (256)



void* tcp_handler( void *arg )
{
    int                status;
    int                clientfd;
    int                server;
    struct sockaddr_in localAddr;
    struct sockaddr_in clientAddr;
    int                optval;
    int                optlen = sizeof( optval );
    socklen_t          addrlen = sizeof( clientAddr );

    server = sl_Socket( AF_INET, SOCK_STREAM, 0 );
    if (server == -1)
    {
        UART_PRINT( "Test App: telnet socket failed [%d]\n", errno );
        goto shutdown;
    }


    memset( &localAddr, 0, sizeof( localAddr ) );
    localAddr.sin_family = AF_INET;
    localAddr.sin_addr.s_addr = htonl( INADDR_ANY );
    localAddr.sin_port = htons( TELNET_PORT );

    status = sl_Bind( server, (struct sockaddr *)&localAddr, sizeof( localAddr ) );
    if (status == -1)
    {
        UART_PRINT( "Test App: telnet bind failed\n" );
        goto shutdown;
    }

    status = sl_Listen( server, NUMTCPWORKERS );
    if (status == -1)
    {
        UART_PRINT( "Test App: telnet listen failed\n" );
        goto shutdown;
    }

    optval = 1;
    status = sl_SetSockOpt( server, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen );
    if (status == -1)
    {
        UART_PRINT( "Test App: telnet setsockopt failed\n" );
        goto shutdown;
    }

    while ((clientfd = sl_Accept( server, (struct sockaddr *)&clientAddr, &addrlen )) != -1)
    {

        char buffer[TCPPACKETSIZE];
        int temperature = lm75_get_temperature();
        snprintf( buffer, sizeof( buffer ), "temp is %d C\n", temperature );

        sl_Send( clientfd, buffer, strlen( buffer ), 0 );

        sl_Close( clientfd );
    }

    UART_PRINT( "Test App: telnet accept failed.\n" );

shutdown:
    if (server != -1)
    {
        sl_Close( server );
    }

    return (0);
}




