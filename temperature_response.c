
#include "temperature_response.h"
#include "uart_term.h"
#include "lm75.h"

#include <stdio.h>


/* header fields definitions */
#define WEB_SERVER_VERSION                      "HTTP Version:"
#define WEB_SERVER_REQUEST_URI                  "Request URI:"
#define WEB_SERVER_QUERY_STRING                 "Query String:"
#define WEB_SERVER_HEADER_CONTENT_TYPE          "Content-Type: "
#define WEB_SERVER_HEADER_CONTENT_LEN           "Content-Length: "
#define WEB_SERVER_HEADER_LOCATION              "Location: "
#define WEB_SERVER_HEADER_SERVER                "Server: "
#define WEB_SERVER_HEADER_USER_AGENT            "User-Agent: "
#define WEB_SERVER_HEADER_COOKIE                "Cookie:"
#define WEB_SERVER_HEADER_SET_COOKIE            "Set-Cookie: "
#define WEB_SERVER_HEADER_UPGRADE               "Upgrade: "
#define WEB_SERVER_HEADER_REFERER               "Referer: "
#define WEB_SERVER_HEADER_ACCEPT                "Accept: "
#define WEB_SERVER_HEADER_CONTENT_ENCODING      "Content-Encoding: "
#define WEB_SERVER_HEADER_CONTENT_DISPOSITION   "Content-Disposition: "
#define WEB_SERVER_HEADER_CONNECTION            "Connection: "
#define WEB_SERVER_HEADER_ETAG                  "Etag: "
#define WEB_SERVER_HEADER_DATE                  "Date: "
#define WEB_SERVER_HEADER_HOST                  "Host: "
#define WEB_SERVER_HEADER_ACCEPT_ENCODING       "Accept-Encoding: "
#define WEB_SERVER_HEADER_ACCEPT_LANGUAGE       "Accept-Language: "
#define WEB_SERVER_HEADER_CONTENT_LANGUAGE      "Content-Language: "

/* Content types list */
#define TEXT_HTML                    "text/html"
#define TEXT_CSS                     "text/css"
#define TEXT_XML                     "text/xml"
#define APPLICATION_JSON             "application/json"
#define IMAGE_PNG                    "image/png"
#define IMAGE_GIF                    "image/gif"
#define TEXT_PLAIN                   "text/plain"
#define TEXT_CSV                     "text/csv"
#define APPLICATION_JAVASCRIPT       "application/javascript"
#define IMAGE_JPEG                   "image/jpeg"
#define APPLICATION_PDF              "application/pdf"
#define APPLICATION_ZIP              "application/zip"
#define SHOCKWAVE_FLASH              "application/x-shockwave-flash"
#define AUDIO_X_AAC                  "audio/x-aac"
#define IMAGE_X_ICON                 "image/x-icon"
#define TEXT_VCARD                   "text/vcard"
#define APPLICATION_OCTEC_STREAM     "application/octet-stream"
#define VIDEO_AVI                    "video/avi"
#define VIDEO_MPEG                   "video/mpeg"
#define VIDEO_MP4                    "video/mp4"
#define FORM_URLENCODED              "application/x-www-form-urlencoded"

/* MIME types list */
#define TEXT_HTML_MIME                  ".html"
#define TEXT_CSS_MIME                   ".css"
#define TEXT_XML_MIME                   ".xml"
#define APPLICATION_JSON_MIME           ".json"
#define IMAGE_PNG_MIME                  ".png"
#define IMAGE_GIF_MIME                  ".gif"
#define TEXT_PLAIN_MIME                 ".txt"
#define TEXT_CSV_MIME                   ".csv"
#define APPLICATION_JAVASCRIPT_MIME     ".js"
#define IMAGE_JPEG_MIME                 ".jpg"
#define APPLICATION_PDF_MIME            ".pdf"
#define APPLICATION_ZIP_MIME            ".zip"
#define SHOCKWAVE_FLASH_MIME            ".swf"
#define AUDIO_X_AAC_MIME                ".aac"
#define IMAGE_X_ICON_MIME               ".ico"
#define TEXT_VCARD_MIME                 ".vcf"
#define APPLICATION_OCTEC_STREAM_MIME   ".bin"
#define VIDEO_AVI_MIME                  ".avi"
#define VIDEO_MPEG_MIME                 ".mpeg"
#define VIDEO_MP4_MIME                  ".mp4"
#define URL_ENCODED_MIME                ".form"    /* dummy - no such extension */

#define NETAPP_MAX_METADATA_LEN        (100)
#define NETAPP_MAX_RX_FRAGMENT_LEN     SL_NETAPP_REQUEST_MAX_DATA_LEN


typedef enum
{
/* Content types list */
    HttpContentTypeList_TextHtml,
    HttpContentTypeList_TextCSS,
    HttpContentTypeList_TextXML,
    HttpContentTypeList_ApplicationJson,
    HttpContentTypeList_ImagePNG,
    HttpContentTypeList_ImageGIF,
    HttpContentTypeList_TextPlain,
    HttpContentTypeList_TextCSV,
    HttpContentTypeList_ApplicationJavascript,
    HttpContentTypeList_ImageJPEG,
    HttpContentTypeList_ApplicationPDF,
    HttpContentTypeList_ApplicationZIP,
    HttpContentTypeList_ShokewaveFlash,
    HttpContentTypeList_AudioXAAC,
    HttpContentTypeList_ImageXIcon,
    HttpContentTypeList_TextVcard,
    HttpContentTypeList_ApplicationOctecStream,
    HttpContentTypeList_VideoAVI,
    HttpContentTypeList_VideoMPEG,
    HttpContentTypeList_VideoMP4,
    HttpContentTypeList_UrlEncoded,
} HttpContentTypeList;

typedef struct    _http_contentTypeMapping_t_
{
    HttpContentTypeList contentType;
    char *contentTypeText;
    char *mimeExt;
}http_contentTypeMapping_t;

http_contentTypeMapping_t g_ContentTypes [] =
{
    {HttpContentTypeList_TextHtml, TEXT_HTML, TEXT_HTML_MIME},
    {HttpContentTypeList_TextCSS, TEXT_CSS, TEXT_CSS_MIME},
    {HttpContentTypeList_TextXML, TEXT_XML, TEXT_XML_MIME},
    {HttpContentTypeList_ApplicationJson, APPLICATION_JSON,
     APPLICATION_JSON_MIME},
    {HttpContentTypeList_ImagePNG, IMAGE_PNG, IMAGE_PNG_MIME},
    {HttpContentTypeList_ImageGIF, IMAGE_GIF, IMAGE_GIF_MIME},
    {HttpContentTypeList_TextPlain, TEXT_PLAIN, TEXT_PLAIN_MIME},
    {HttpContentTypeList_TextCSV, TEXT_CSV, TEXT_CSV_MIME},
    {HttpContentTypeList_ApplicationJavascript, APPLICATION_JAVASCRIPT,
     APPLICATION_JAVASCRIPT_MIME},
    {HttpContentTypeList_ImageJPEG, IMAGE_JPEG, IMAGE_JPEG_MIME},
    {HttpContentTypeList_ApplicationPDF, APPLICATION_PDF,
     APPLICATION_PDF_MIME},
    {HttpContentTypeList_ApplicationZIP, APPLICATION_ZIP,
     APPLICATION_ZIP_MIME},
    {HttpContentTypeList_ShokewaveFlash, SHOCKWAVE_FLASH,
     SHOCKWAVE_FLASH_MIME},
    {HttpContentTypeList_AudioXAAC, AUDIO_X_AAC, AUDIO_X_AAC_MIME},
    {HttpContentTypeList_ImageXIcon, IMAGE_X_ICON, IMAGE_X_ICON_MIME},
    {HttpContentTypeList_TextVcard, TEXT_VCARD, TEXT_VCARD_MIME},
    {HttpContentTypeList_ApplicationOctecStream, APPLICATION_OCTEC_STREAM,
     APPLICATION_OCTEC_STREAM_MIME},
    {HttpContentTypeList_VideoAVI, VIDEO_AVI, VIDEO_AVI_MIME},
    {HttpContentTypeList_VideoMPEG, VIDEO_MPEG, VIDEO_MPEG_MIME},
    {HttpContentTypeList_VideoMP4, VIDEO_MP4, VIDEO_MP4_MIME},
    {HttpContentTypeList_UrlEncoded, FORM_URLENCODED, URL_ENCODED_MIME}
};


static uint16_t prepare_metadata( int32_t parsing_status,
                                  uint32_t content_len,
                                  HttpContentTypeList content_type_id,
                                  uint8_t *metadata_buffer )
{
    char *contentType;
    uint8_t *pMetadata;
    uint16_t metadataLen;

    contentType = g_ContentTypes[content_type_id].contentTypeText;

    pMetadata = metadata_buffer;

    /* http status */
    *pMetadata = (uint8_t) SL_NETAPP_REQUEST_METADATA_TYPE_STATUS;
    pMetadata++;
    *(uint16_t *)pMetadata = (uint16_t) 2;
    pMetadata += 2;

    if (parsing_status < 0)
    {
        *(uint16_t *)pMetadata = (uint16_t) SL_NETAPP_HTTP_RESPONSE_404_NOT_FOUND;
    }
    else
    {
        *(uint16_t *)pMetadata = (uint16_t) SL_NETAPP_HTTP_RESPONSE_200_OK;
    }

    pMetadata += 2;

    /* Content type */
    *pMetadata = (uint8_t) SL_NETAPP_REQUEST_METADATA_TYPE_HTTP_CONTENT_TYPE;
    pMetadata++;
    (*(uint16_t *)pMetadata) = (uint16_t) strlen( (const char *)contentType );
    pMetadata += 2;
    sl_Memcpy( pMetadata, contentType, strlen( (const char *)contentType ) );
    pMetadata += strlen( (const char *)contentType );

    /* Content len */
    *pMetadata = SL_NETAPP_REQUEST_METADATA_TYPE_HTTP_CONTENT_LEN;
    pMetadata++;
    *(uint16_t *)pMetadata = (uint16_t) 4;
    pMetadata += 2;
    *(uint32_t *)pMetadata = (uint32_t) content_len;

    metadataLen = 5 + 7 + strlen( (const char *)contentType ) + 3;

    return (metadataLen);
}

void httpGetHandler( const SlNetAppRequest_t *netAppRequest )
{
    uint8_t *pTlv = netAppRequest->requestData.pMetadata;
    uint8_t *pEnd = netAppRequest->requestData.pMetadata + netAppRequest->requestData.MetadataLen;
    uint8_t type;
    uint16_t len;
    const char* service = "/temperature";
    bool is_service_request = false;
    uint16_t metadata_len;

    while(pTlv < pEnd)
    {
        type = *pTlv;
        pTlv++;
        len = *(uint16_t *)pTlv;
        pTlv += 2;

        if (SL_NETAPP_REQUEST_METADATA_TYPE_HTTP_REQUEST_URI == type)
        {
            UART_PRINT( "GET: %s\n", (const char *)pTlv );
            is_service_request = strncmp( (const char *)pTlv, service, strlen( service )) == 0;
        }

        pTlv += len;
    }

    if (is_service_request)
    {
        int temperature = lm75_get_temperature();
        uint8_t metadata_buffer[NETAPP_MAX_METADATA_LEN];
        uint8_t payload_buffer[NETAPP_MAX_RX_FRAGMENT_LEN];

        snprintf( payload_buffer, sizeof( payload_buffer ), "temp is %d C", temperature );

        metadata_len = prepare_metadata( 0, strlen( payload_buffer ), HttpContentTypeList_TextPlain, metadata_buffer );

        sl_NetAppSend( netAppRequest->Handle, metadata_len, metadata_buffer,
                       (SL_NETAPP_REQUEST_RESPONSE_FLAGS_CONTINUATION | SL_NETAPP_REQUEST_RESPONSE_FLAGS_METADATA) );

        sl_NetAppSend( netAppRequest->Handle,
                       /* mark as last segment */
                       strlen( (const char *)payload_buffer ), payload_buffer, 0 );
    }
}
