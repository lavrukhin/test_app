
#include "lm75.h"
#include "wlan.h"
#include "temperature_response.h"
#include "uart_term.h"

#include <ti/drivers/net/wifi/simplelink.h>

#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <mqueue.h>

#include <ti_drivers_config.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/Power.h>



#define SPAWN_TASK_PRIORITY     (9)
#define TASK_STACK_SIZE         (2048)

pthread_t gSpawn_thread = (pthread_t)NULL;
pthread_t tcp_thread    = (pthread_t)NULL;

static mqd_t linkLocalMQueue;

extern void* tcp_handler( void *arg );


void * mainThread( void *arg )
{
    int32_t             ret ;
    pthread_attr_t      pAttrs_spawn;
    pthread_attr_t      pAttrs_telnet;
    struct sched_param  priParam;
    struct timespec     ts = {0};
    mq_attr             attr;

    /* Initializes the SPI interface to the Network
       Processor and peripheral SPI (if defined in the board file) */
    SPI_init();
    GPIO_init();
    I2C_init();

    InitTerm();

    /* initialize the realtime clock */
    clock_settime( CLOCK_REALTIME, &ts );

    /* Switch off all LEDs on boards */
    GPIO_write( CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_OFF );
    //GPIO_write( CONFIG_GPIO_LED_1, CONFIG_GPIO_LED_ON );
    //GPIO_write( CONFIG_GPIO_LED_2, CONFIG_GPIO_LED_OFF );

    /* Create the sl_Task internal spawn thread */
    pthread_attr_init( &pAttrs_spawn );
    priParam.sched_priority = SPAWN_TASK_PRIORITY;
    ret = pthread_attr_setschedparam( &pAttrs_spawn, &priParam );
    ret |= pthread_attr_setstacksize( &pAttrs_spawn, TASK_STACK_SIZE );

    /* The SimpleLink host driver architecture mandate spawn
       thread to be created prior to calling Sl_start (turning the NWP on). */
    /* The purpose of this thread is to handle
       asynchronous events sent from the NWP.
     * Every event is classified and later handled
       by the Host driver event handlers. */
    ret = pthread_create( &gSpawn_thread, &pAttrs_spawn, sl_Task, NULL );
    if (ret != 0)
    {
        /* Handle Error */
        UART_PRINT( "Test App - Unable to create spawn thread \n" );
        return ( NULL );
    }

    /* Before turning on the NWP on,
       reset any previously configured parameters */
    /*
         IMPORTANT NOTE - This is an example reset function,
         user must update this function to match the application settings.
     */
    ret = sl_WifiConfig();
    if (ret < 0)
    {
        /* Handle Error */
        UART_PRINT( "Test App - Couldn't configure Network Processor - %d\n", ret );
        return ( NULL );
    }

    /* Turn NWP on */
    ret = sl_Start( NULL, NULL, NULL );
    if (ret < 0)
    {
        /* Handle Error */
        UART_PRINT( "sl_start failed - %d\n", ret );
        return ( NULL );
    }

    wlan_start();


    pthread_attr_init( &pAttrs_telnet );
    priParam.sched_priority = 2;
    ret = pthread_attr_setschedparam( &pAttrs_telnet, &priParam );
    ret |= pthread_attr_setstacksize( &pAttrs_telnet, TASK_STACK_SIZE );
    ret = pthread_create( &tcp_thread, &pAttrs_telnet, tcp_handler, NULL );
    if (ret != 0)
    {
        UART_PRINT( "Test App - Unable to create spawn thread \n" );
        return( NULL );
    }


    /* initializes mailbox for http messages */
    attr.mq_maxmsg = 10;         /* queue size */
    attr.mq_msgsize = sizeof( SlNetAppRequest_t* );        /* Size of message */
    linkLocalMQueue = mq_open( "linklocal msg q", O_CREAT, 0, &attr );
    if (((int)linkLocalMQueue) <= 0)
    {
        UART_PRINT( "[Link local task] could not create msg queue\n\r" );
        while(1)
        {
            ;
        }
    }

    lm75_init();

    while(1)
    {
        SlNetAppRequest_t *netAppRequest;
        int32_t msgqret;

        msgqret = mq_receive( linkLocalMQueue, (char *)&netAppRequest,
                                 sizeof(SlNetAppRequest_t*), NULL);
        if (msgqret < 0)
        {
            UART_PRINT( "[Link local task] could not receive element from msg queue\n\r" );
            while(1) {;}
        }

        if (netAppRequest->Type == SL_NETAPP_REQUEST_HTTP_GET)
        {
            httpGetHandler( netAppRequest );
        }

        if (netAppRequest->requestData.MetadataLen > 0)
        {
            free( netAppRequest->requestData.pMetadata );
        }
        if (netAppRequest->requestData.PayloadLen > 0)
        {
            free( netAppRequest->requestData.pPayload );
        }

        free( netAppRequest );
    }

    return(0);
}


/*!
    \brief          SimpleLinkFatalErrorEventHandler

    This handler gets called whenever a socket event is reported
    by the NWP / Host driver. After this routine is called, the user's
    application must restart the device in order to recover.

    \param          slFatalErrorEvent    -   pointer to fatal error event.

    \return         void

    \note           For more information, please refer to:
                    user.h in the porting
                    folder of the host driver and the
                    CC31xx/CC32xx NWP programmer's
                    guide (SWRU455).

*/
void SimpleLinkFatalErrorEventHandler(SlDeviceFatal_t *slFatalErrorEvent)
{

}

/*!
    \brief          SimpleLinkGeneralEventHandler

    This handler gets called whenever a general error is reported
    by the NWP / Host driver. Since these errors are not fatal,
    application can handle them.

    \param          pDevEvent    -   pointer to device error event.

    \return         void

    \note           For more information, please refer to:
                    user.h in the porting
                    folder of the host driver and the
                    CC31xx/CC32xx NWP programmer's
                    guide (SWRU455).

*/
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{

}

/*!
    \brief          SimpleLinkSockEventHandler

    This handler gets called whenever a socket event is reported
    by the NWP / Host driver.

    \param          SlSockEvent_t    -   pointer to socket event data.

    \return         void

    \note           For more information, please refer to:
                    user.h in the porting
                    folder of the host driver and the
                    CC31xx/CC32xx NWP programmer's
                    guide (SWRU455).

*/
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{

}

/*!
    \brief          SimpleLinkHttpServerEventHandler

    This handler gets called whenever a HTTP event is reported
    by the NWP internal HTTP server.

    \param          pHttpEvent       -   pointer to http event data.

    \param          pHttpEvent       -   pointer to http response.

    \return         void

    \note           For more information, please refer to: user.h in the porting
                    folder of the host driver and the  CC31xx/CC32xx NWP
                    programmer's guide (SWRU455).

 */
void SimpleLinkHttpServerEventHandler( SlNetAppHttpServerEvent_t *pHttpEvent,
                                       SlNetAppHttpServerResponse_t *
                                       pHttpResponse)
{

}

/*!
    \brief          SimpleLinkNetAppEventHandler

    This handler gets called whenever a Netapp event is reported
    by the host driver / NWP. Here user can implement he's own logic
    for any of these events. This handler is used by 'network_terminal'
    application to show case the following scenarios:

    1. Handling IPv4 / IPv6 IP address acquisition.
    2. Handling IPv4 / IPv6 IP address Dropping.

    \param          pNetAppEvent     -   pointer to Netapp event data.

    \return         void

    \note           For more information, please refer to:
                    user.h in the porting
                    folder of the host driver and the
                    CC31xx/CC32xx NWP programmer's
                    guide (SWRU455).

*/
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{

}


static void NetAppRequestErrorResponse( SlNetAppResponse_t *pNetAppResponse )
{
    UART_PRINT( "Could not allocate memory for netapp request\n\r" );

    /* Prepare error response */
    pNetAppResponse->Status = SL_NETAPP_RESPONSE_NONE;
    pNetAppResponse->ResponseData.pMetadata = NULL;
    pNetAppResponse->ResponseData.MetadataLen = 0;
    pNetAppResponse->ResponseData.pPayload = NULL;
    pNetAppResponse->ResponseData.PayloadLen = 0;
    pNetAppResponse->ResponseData.Flags = 0;
}

/*!
    \brief          SimpleLinkNetAppRequestEventHandler

    This handler gets called whenever a NetApp event is reported
    by the NWP / Host driver. User can write he's logic to handle
    the event here.

    \param          pNetAppRequest     -   Pointer to NetApp request structure.

    \param          pNetAppResponse    -   Pointer to NetApp request Response.

    \note           For more information, please refer to:
                    user.h in the porting
                    folder of the host driver and the
                    CC31xx/CC32xx NWP programmer's
                    guide (SWRU455).

    \return         void

 */
void SimpleLinkNetAppRequestEventHandler(SlNetAppRequest_t *pNetAppRequest,
                                         SlNetAppResponse_t *pNetAppResponse)
{
    SlNetAppRequest_t *netAppRequest;
    int32_t msgqret;

    UART_PRINT( "[Test App] NetApp Request Received - AppId = %d, Type = %d, Handle = %d\n\r",
                pNetAppRequest->AppId, pNetAppRequest->Type, pNetAppRequest->Handle );

    if ((pNetAppRequest->Type == SL_NETAPP_REQUEST_HTTP_GET) ||
            (pNetAppRequest->Type == SL_NETAPP_REQUEST_HTTP_DELETE) ||
            (pNetAppRequest->Type == SL_NETAPP_REQUEST_HTTP_POST) ||
            (pNetAppRequest->Type == SL_NETAPP_REQUEST_HTTP_PUT))
    {
        /* Prepare pending response */
        pNetAppResponse->Status = SL_NETAPP_RESPONSE_PENDING;
        pNetAppResponse->ResponseData.pMetadata = NULL;
        pNetAppResponse->ResponseData.MetadataLen = 0;
        pNetAppResponse->ResponseData.pPayload = NULL;
        pNetAppResponse->ResponseData.PayloadLen = 0;
        pNetAppResponse->ResponseData.Flags = 0;
    }
    else
    {
        NetAppRequestErrorResponse( pNetAppResponse );

        return;
    }

    netAppRequest = (SlNetAppRequest_t *) malloc( sizeof( SlNetAppRequest_t ) );
    if (NULL == netAppRequest)
    {
        NetAppRequestErrorResponse( pNetAppResponse );
        return;
    }

    memset( netAppRequest, 0, sizeof( SlNetAppRequest_t ) );
    netAppRequest->AppId = pNetAppRequest->AppId;
    netAppRequest->Type = pNetAppRequest->Type;
    netAppRequest->Handle = pNetAppRequest->Handle;
    netAppRequest->requestData.Flags = pNetAppRequest->requestData.Flags;

    /* Copy Metadata */
    if (pNetAppRequest->requestData.MetadataLen > 0)
    {
        netAppRequest->requestData.pMetadata = (uint8_t *) malloc( pNetAppRequest->requestData.MetadataLen );
        if (NULL == netAppRequest->requestData.pMetadata)
        {
            NetAppRequestErrorResponse( pNetAppResponse );
            free( netAppRequest );
            return;
        }
        sl_Memcpy( netAppRequest->requestData.pMetadata,
                   pNetAppRequest->requestData.pMetadata,
                   pNetAppRequest->requestData.MetadataLen  );
        netAppRequest->requestData.MetadataLen = pNetAppRequest->requestData.MetadataLen;
    }
    else
    {
        netAppRequest->requestData.MetadataLen = 0;
    }

    /* Copy the payload */
    if (pNetAppRequest->requestData.PayloadLen > 0)
    {
        netAppRequest->requestData.pPayload = (uint8_t *) malloc( pNetAppRequest->requestData.PayloadLen );
        if (NULL == netAppRequest->requestData.pPayload)
        {
            NetAppRequestErrorResponse( pNetAppResponse );

            if (netAppRequest->requestData.pMetadata != NULL)
            {
                free( netAppRequest->requestData.pMetadata );
            }
            free( netAppRequest );
            return;
        }
        sl_Memcpy( netAppRequest->requestData.pPayload,
                   pNetAppRequest->requestData.pPayload,
                   pNetAppRequest->requestData.PayloadLen );
        netAppRequest->requestData.PayloadLen = pNetAppRequest->requestData.PayloadLen;
    }
    else
    {
        netAppRequest->requestData.PayloadLen = 0;
    }

    msgqret = mq_send( linkLocalMQueue, (char *)&netAppRequest, 1, 0 );
    if(msgqret < 0)
    {
        UART_PRINT("[Link local task] could not send element to msg queue\n\r");
        while(1)
        {
            ;
        }
    }
}

void SimpleLinkNetAppRequestMemFreeEventHandler(uint8_t *buffer)
{

}

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{

}
